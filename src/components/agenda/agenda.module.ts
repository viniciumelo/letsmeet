import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendaComponent } from './agenda';

@NgModule({
  declarations: [
    AgendaComponent,
  ],
  imports: [
    IonicPageModule.forChild(AgendaComponent),
  ],
  exports: [
    AgendaComponent
  ]
})
export class AgendaComponentModule {}
