import { ToastService } from '../../providers/toast/toast.service';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth-provider';
//import {Observable} from "rxjs/Observable";
import {UserProvider} from '../../providers/user/user-provider';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  following = false;
  
  public myUser: any;
  public user: any;
  constructor(public authProvider: AuthProvider,
              public userProvider: UserProvider,
              public nav: NavController,
              public toastCtrl: ToastService) {

    //myUser is an observable
    this.myUser = this.userProvider.getUserObject(); //null if not logged in
    console.log('just got getCurrentUser');
    this.myUser.subscribe(user => {
      // console.log(user);
      this.user = user;
    });
  }
  
  logoutFromAbout(): void {
    this.authProvider.logout();
    console.log('desconectado!');
  }

  follow() {
    this.following = !this.following;
    this.toastCtrl.create('Follow user clicked');
  }

 

}
