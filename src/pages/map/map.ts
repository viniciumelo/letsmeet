

import { Component } from '@angular/core';
import { ToastController, ModalController } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  Geocoder,
  GeocoderResult
} from '@ionic-native/google-maps';
import { HeaderColor } from '@ionic-native/header-color'; //adicionando cor a janela do app

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})


export class MapPage {

  map: GoogleMap;
  search_address: any;
  isRunning: boolean = false;

  constructor(public toastCtrl: ToastController, private headerColor: HeaderColor, private modal: ModalController) { }

  ionViewDidLoad() {
    this.headerColor.tint('#000000'); //adicionando cor a janela do app
    this.loadMap();
  }

  loadMap() {
    this.createMaps();
    this.updateMaps();
  }

  createMaps() {
    // Create a map after the view is loaded.
    // (platform is already ready in app.component.ts)
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 53.0741704,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    });
  }

  //funcao não utilizada
  searchMaps(locationSrc) {
    this.search_address = locationSrc;
  }

  //quando clica no botao de pesquisar
  onButton_click() {
    this.search_address = "Kyoto, Japan";
    // Address -> latitude,longitude
    Geocoder.geocode({
      "address": this.search_address
    }).then((results: GeocoderResult[]) => {
      console.log(results);

      if (!results.length) {
        this.isRunning = false;
        return null;
      }

      // Add a marker
      let marker: Marker = this.map.addMarkerSync({
        'position': results[0].position,
        'title': JSON.stringify(results[0].position)
      });

      // Move to the position
      this.map.animateCamera({
        'target': marker.getPosition(),
        'zoom': 17
      }).then(() => {
        marker.showInfoWindow();
        this.isRunning = false;
      });
    });

  }

  updateMaps() {
    this.map.clear();

    // Get the location of you
    this.map.getMyLocation()
      .then((location: MyLocation) => {
        console.log(JSON.stringify(location, null, 2));

        // Move the map camera to the location with animation
        this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30
        })
          .then(() => { //Localização atual

            let marker: Marker = this.map.addMarkerSync({
              title: 'Estou aqui!',
              snippet: 'Clique no marcador.',
              icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
              position: location.latLng,
              animation: GoogleMapsAnimation.BOUNCE
            });

            // show the infoWindow
            marker.showInfoWindow();

          }).then(() => { //Localização dos eventos

            var locations = [
              { lat: -15.792536, lng: -47.986678 },
              { lat: -15.791452, lng: -47.984173 },
              { lat: -15.788936, lng: -47.989286 },
              { lat: -15.789807, lng: -47.985749 },
              { lat: -15.791902, lng: -47.988290 },
            ];

            var imgEvent = 'assets/img/pin.png';


            var eventName = [
              { evento: "Evento 1" },
              { evento: "Evento 2" },
              { evento: "Evento 3" },
              { evento: "Evento 4" },
              { evento: "Evento 5" },
            ];

            //cada elemento do json marcar no mapa
            for (var i = 0; i < locations.length; i++) {

              // let locationEvent = [{ lat: locations[i].lat, lng: locations[i].lng }]
              let marker: Marker = this.map.addMarkerSync({
                title: eventName[i].evento + " ",
                content: imgEvent,
                snippet: '',
                icon: 'assets/img/pin.png',
                position: locations[i],
                // position: locationEvent[i],
              });

              marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {  //nesse marker o evento é adicionar no info
                // show the infoWindow
                marker.showInfoWindow();//info com o title
              });
              marker.on(GoogleMapsEvent.INFO_CLICK).subscribe(() => {
                this.showToast('Carregando o ' + marker.getTitle());
                this.openModal(marker.getTitle());
              });
            }
          });
      });
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1000,
      position: 'middle'
    });
    toast.present(toast);
  }

  openModal(nameEvent) {
    const myModal = this.modal.create('ModalPage', { data: nameEvent });
    myModal.present();
  }
}