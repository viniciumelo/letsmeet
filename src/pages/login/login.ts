import { Component, ViewChild  } from '@angular/core';
import { AlertController, App, LoadingController, NavController, Loading, MenuController  } from 'ionic-angular';
import {FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms';
import { SignupPage } from '../../pages/signup/signup';
import { ResetPasswordPage } from '../reset-password/reset-password';

import { AuthProvider } from '../../providers/auth/auth-provider';
import {ProfileDataProvider}  from '../../providers/profile/profile-provider';




@Component({
  selector: 'page-user',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  error: any;

  signupPage = SignupPage;
  resetPasswordPage = ResetPasswordPage; //Added reset password page
  public loading:Loading;
  
  public backgroundImage = 'assets/img/background-5.jpg';

  @ViewChild('player') player;  
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public menu: MenuController,
    public app: App,
    public authProvider: AuthProvider,
    public firedataProvider: ProfileDataProvider,
    public fb: FormBuilder,
    public navCtrl: NavController
  ) { 
    this.navCtrl = navCtrl;
    this.menu = menu;
    this.menu.enable(false, 'myMenu');
    this.loginForm = this.fb.group({
    'email': ['', Validators.compose([Validators.required, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
    'password': ['', Validators.compose([Validators.required, Validators.minLength(1)])]
  });
  this.email = this.loginForm.controls['email'];
  this.password = this.loginForm.controls['password'];

}


  ionViewWillLeave() {
    // the .nativeElement property of the ViewChild is the reference to the tag <video>
    this.player.nativeElement.src = '';
    this.player.nativeElement.load();
  }

  ionViewWillEnter() {
    this.player.nativeElement.src = 'assets/video/background-480.mp4';
    this.player.nativeElement.load();
  }

  login(method): void {

    this.authProvider.login(method, this.email.value, this.password.value).subscribe(data =>{
      //Successfully logged in user
      console.log(data);

      //need to get a properly formatted user object here to pass to writeUserToDatabase
      this.firedataProvider.writeUserToDatabase();
      this.loading.dismiss();
      console.log('Usuário está conectado.');
    }, error => {

      //error has occurred with login
      console.log(error);
      this.loading.dismiss();
      {alert(error.message)}
      this.error = error;
    });

    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Login no usuário',
      showBackdrop: false
    });
    this.loading.present();

  }

}
