import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from '../../providers/auth/auth-provider';
import { UserProvider } from '../../providers/user/user-provider';
import { FirebaseListObservable } from 'angularfire2/database';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { AgendaComponent } from '../../components/agenda/agenda';
import * as firebase from 'firebase';
/*
  Generated class for the AgendaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AgendaProvider {

  public agendaList: FirebaseListObservable<any>;
  public agendaDetail: FirebaseObjectObservable<any>;
  public userId: string;
  public agendamentos: FirebaseListObservable<any>;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;

  constructor(public afDatabase: AngularFireDatabase,
              public authProvider: AuthProvider,
              public userProvider: UserProvider,
              public afAuth: AngularFireAuth,
              public agenda: AgendaComponent)
{
      this.afAuth.authState.subscribe( user => {
        this.userId = user.uid;
        this.agendaList = this.afDatabase.list(`/agendamentos/${user.uid}/agendaList`);
  });

  console.log('Hello AgendaProvider Provider');
  }

  




    getAgendaList(): FirebaseListObservable<any> {
    return this.agendaList;
  }

  getAgenda(agendaId: string): FirebaseObjectObservable<any> {
    return this.agendaDetail = this.afDatabase
      .object(`/userProfile/${this.userId}/agendaList/${agendaId}`);
  }

  createAgenda(name: string, amount: number, dueDate: string = null,
             paid: boolean = false):firebase.Promise<any>{
    return this.agendaList.push({ name, amount, dueDate, paid });
  }

  removeAgenda(agendaId: string): firebase.Promise<any> {
    return this.agendaList.remove(agendaId);
  }


  takeMessagePhoto(agendaId: string, imageURL: string): any {
    const storageRef = firebase.storage().ref(this.userId);
    return storageRef.child(agendaId).child('agendaPicture')
      .putString(imageURL, 'base64', {contentType: 'image/png'})
      .then( pictureSnapshot => {
        this.agendaList.update(agendaId, { picture: pictureSnapshot.downloadURL });
      });
  }




}
